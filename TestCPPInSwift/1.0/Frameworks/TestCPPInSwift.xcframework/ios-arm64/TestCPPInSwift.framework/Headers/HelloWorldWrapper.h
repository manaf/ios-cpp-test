//
//  HelloWorldWrapper.h
//  TestCPPInSwift
//
//  Created by Firas Ataya on 07/03/2021.
//

#ifndef HelloWorldWrapper_h
#define HelloWorldWrapper_h

#import <Foundation/Foundation.h>
@interface HelloWorldWrapper : NSObject
- (NSString *) sayHello;
@end

#endif /* HelloWorldWrapper_h */
