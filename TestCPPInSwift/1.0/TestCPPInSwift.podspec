Pod::Spec.new do |spec|
    spec.name                  = 'TestCPPInSwift'
    spec.version               = '1.0'
    spec.summary               = 'Medicus Core Library'
    spec.description           = <<-DESC
    'Medicus Core Library with c++ headers'
    DESC
    spec.homepage              = 'https://Medicus.ai'
    spec.license               = {
        :type => 'Copyright',
        :text => <<-LICENCE
        Copyright 2020 Medicus AI GmbH. All rights reserved.
        LICENCE
    }
    spec.author                = { 'Medicus AI' => 'dev@medicus.ai' }
    spec.source                = { :git => 'git@gitlab.com:manaf/ios-cpp-test.git'}
    spec.vendored_frameworks    = 'TestCPPInSwift/1.0/Frameworks/TestCPPInSwift.xcframework'
    spec.pod_target_xcconfig = { 'VALID_ARCHS' => 'arm64 x86_64' }
    end
    